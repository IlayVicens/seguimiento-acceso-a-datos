package com.ilayv.productomvc.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

//en esta clase encontramos los metodos que realizan funciones--> ya sean asignados a un boton o a una accion
public class Util {

    //metodo que devuelve un mensaje de error
    public static void mensajeError(String mensaje){
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);

    }


    //metodo que devuelve un mensaje de confirmacion al pulsar si o no
    public static int mensajeConfirmacion(String mensaje,String titulo){
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_NO_OPTION);

    }

    //metodo que crea el selector de fichero--- el fichero de guardar
    public static JFileChooser crearSelectorFichero(File rutaDefecto,
                                                    String tipoArchivos,
                                                    String extension){
        JFileChooser selectorFichero = new JFileChooser();
        if (rutaDefecto!=null){
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if (extension!=null){
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }


    //metodo que devuelve un mensaje de entrada al pulsar la opcion SI
    public static int mensajeEntrada(String mensaje, String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.YES_OPTION);
    }

    //metodo que devuelve un mensaje de confirmacion al pulsar el boton cerrar
    public static int botonCerrar(String mensaje,String titulo) {
        return JOptionPane.showConfirmDialog(null,mensaje,titulo,JOptionPane.OK_OPTION);
    }









}
