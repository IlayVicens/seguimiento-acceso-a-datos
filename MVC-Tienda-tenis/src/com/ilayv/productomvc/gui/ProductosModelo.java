package com.ilayv.productomvc.gui;

import com.ilayv.productomvc.base.Producto;
import com.ilayv.productomvc.base.Raqueta;
import com.ilayv.productomvc.base.Ropa;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ProductosModelo {
    //creamos un Array de productos
    private ArrayList<Producto> listaProductos;

    //creamos el constructor con el nuevo array
    public ProductosModelo() {
        listaProductos = new ArrayList<Producto>();
    }

    public ArrayList<Producto> obtenerProductos() {
        return listaProductos;
    }

    //creamos el metodo altaRaqueta--> en el cual le pasamos los campos heredados y el propio
    //creamos una nueva Raqueta
    //y añadimos dicha raqueta a la lista de productos
    public void altaRaqueta(String id, String marca, String color,
                            LocalDate fechaCompra, double longitudRaqueta) {
        Raqueta nuevaRaqueta = new Raqueta(id, marca, color, fechaCompra, longitudRaqueta);
        listaProductos.add(nuevaRaqueta);
    }

    //creamos el metodo altaRopa--> en el cual le pasamos los campos heredados y el propio
    //creamos una nueva Ropa
    //y añadimos dicha ropa a la lista de productos
    public void altaRopa(String id, String marca, String color,
                         LocalDate fechaCompra, String talla) {
        Ropa nuevaRopa = new Ropa(id, marca, color, fechaCompra, talla);
        listaProductos.add(nuevaRopa);
    }

    //creamos el metodo si existeID--> a traves del cual comparamos dentro de la lista de productos por el campo ID, comun a todas clases.
    public boolean existeId(String id) {
        for (Producto unProducto : listaProductos) {
            if (unProducto.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    //metodo exportar el XML--> donde configuramos para poder exportar los nuevos productos a un fichero
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //añado el nodo raiz - la primera etiqueta(contiene las demas)
        Element raiz = documento.createElement("Productos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoProducto = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Producto unProducto : listaProductos) {
            //añado dentro de la etiqueta raiz Productos
            //en funcion del tipo de producto
            //raqueta o ropa
            if (unProducto instanceof Raqueta) {
                nodoProducto = documento.createElement("Raqueta");
            } else {
                nodoProducto = documento.createElement("Ropa");
            }
            raiz.appendChild(nodoProducto);

            //dentro de la etiqueta producto
            //las subetiquetas(id,marca...)

            nodoDatos = documento.createElement("id");
            nodoProducto.appendChild(nodoDatos);
            texto = documento.createTextNode(unProducto.getId());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("marca");
            nodoProducto.appendChild(nodoDatos);
            texto = documento.createTextNode(unProducto.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("color");
            nodoProducto.appendChild(nodoDatos);
            texto = documento.createTextNode(unProducto.getColor());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-compra");
            nodoProducto.appendChild(nodoDatos);
            texto = documento.createTextNode(unProducto.getFechaCompra().toString());
            nodoDatos.appendChild(texto);

            //como hay un dato que depende del tipo de producto
            //tengo que controlar el tipo de objeto
            if (unProducto instanceof Raqueta) {
                nodoDatos = documento.createElement("longitud-raqueta");
                nodoProducto.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Raqueta) unProducto).getLongitudRaqueta()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("talla");
                nodoProducto.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Ropa) unProducto).getTalla()));
                nodoDatos.appendChild(texto);
            }

            //guardo los datos en un fichero
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        }
    }

    //metodo para importar un XML--> pasar a un fichero los datos dados de alta en la interfaz grafica
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaProductos = new ArrayList<Producto>();
        Raqueta nuevaRaqueta = null;
        Ropa nuevaRopa = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoProducto = (Element) listaElementos.item(i);

            if (nodoProducto.getTagName().equals("Raqueta")) {
                nuevaRaqueta = new Raqueta();
                nuevaRaqueta.setId(nodoProducto.getChildNodes().item(0).getTextContent());
                nuevaRaqueta.setMarca(nodoProducto.getChildNodes().item(1).getTextContent());
                nuevaRaqueta.setColor(nodoProducto.getChildNodes().item(2).getTextContent());
                nuevaRaqueta.setFechaCompra(LocalDate.parse(nodoProducto.getChildNodes().item(3).getTextContent()));
                nuevaRaqueta.setLongitudRaqueta(Double.parseDouble(nodoProducto.getChildNodes().item(4).getTextContent()));
                listaProductos.add(nuevaRaqueta);
            } else {
                if (nodoProducto.getTagName().equals("Ropa")) {
                    nuevaRopa = new Ropa();
                    nuevaRopa.setId(nodoProducto.getChildNodes().item(0).getTextContent());
                    nuevaRopa.setMarca(nodoProducto.getChildNodes().item(1).getTextContent());
                    nuevaRopa.setColor(nodoProducto.getChildNodes().item(2).getTextContent());
                    nuevaRopa.setFechaCompra(LocalDate.parse(nodoProducto.getChildNodes().item(3).getTextContent()));
                    nuevaRopa.setTalla(nodoProducto.getChildNodes().item(4).getTextContent());
                    listaProductos.add(nuevaRopa);
                }
            }
        }

    }
}
