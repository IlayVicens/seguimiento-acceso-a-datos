package com.ilayv.productomvc.gui;

import com.ilayv.productomvc.Ventana;
import com.ilayv.productomvc.base.Producto;
import com.ilayv.productomvc.base.Raqueta;
import com.ilayv.productomvc.base.Ropa;
import org.xml.sax.SAXException;
import com.ilayv.productomvc.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class ProductosControlador implements ActionListener, ListSelectionListener, WindowListener {
    //cogemos los objetos vista,modelo y la ultimaRutaExportada
    private Ventana vista;
    private ProductosModelo modelo;
    private File ultimaRutaExportada;

    //creamos el constructor y le damos nombre a los atributos vista y modelo
    public ProductosControlador(Ventana vista, ProductosModelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        try{
            cargarDatosConfiguracion();
        }catch (IOException e){
            System.out.println("No existe el fichero de configuracion"+e.getMessage());
        }

        //añadimos los listener
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    //metodo donde cargamos los datos en un fichero---> productos.conf
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("productos.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    //metodo donde creamos la ultimaRutaExportada
    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada=ultimaRutaExportada;
    }

    //metodo donde guardamos la ruta de configuracion--> productos.conf
    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada",ultimaRutaExportada.getAbsolutePath());

        configuracion.store(new PrintWriter("productos.conf"),"Datos configuracion productos");
    }





//se usa para detectar y manejar eventos, los cuales tienen lugar cuando
// se produce una acción sobre un elemento del programa
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        //damos al boton "nuevo" la funcionalidad
        switch (actionCommand){
            case "Nuevo":
                if(hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \n"+
                            "Id \n Marca \n Color \n Fecha compra\n"+
                            vista.longitudTallaTxt.getText());
                    break;
                }
                if(modelo.existeId(vista.idTxt.getText())){
                    Util.mensajeError("Ya existe un producto con ese id\n"+
                            vista.longitudTallaTxt.getText());
                    break;
                }
                if (vista.raquetaRadioButton.isSelected()){
                    modelo.altaRaqueta(vista.idTxt.getText(),
                    vista.marcaTxt.getText(),
                    vista.colorTxt.getText(),
                    vista.fechaCompraDPicker.getDate(),
                            Double.parseDouble(vista.longitudTallaTxt.getText()));
                }else{
                    modelo.altaRopa(vista.idTxt.getText(),
                            vista.marcaTxt.getText(),
                            vista.colorTxt.getText(),
                            vista.fechaCompraDPicker.getDate(),
                            vista.longitudTallaTxt.getText());
                }
                limpiarCampos();
                refrescar();
                break;
                //damos funcionalidad al boton "Importar"
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int opt=selectorFichero.showOpenDialog(null);
                if(opt==JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    }catch(ParserConfigurationException e1){
                        e1.printStackTrace();
                    }catch (IOException e1){
                        e1.printStackTrace();
                    }catch(SAXException e1){
                        e1.printStackTrace();
                    }
                    refrescar();
                }
                break;
                //damos funcionalidad al boton "Exportar"
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int opt2=selectorFichero2.showSaveDialog(null);
                if (opt2==JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    }catch (ParserConfigurationException e1){
                        e1.printStackTrace();
                    }catch (TransformerException e1){
                        e1.printStackTrace();
                    }
                }
                break;
                //si selecionamos el radioButton de ropa--> aparece el campo talla para rellenar
            case "Ropa":
                vista.tallaLongitudLbl.setText("Talla");
                break;
                // si seleccionamos el radioButton de Raqueta--> aparece el campo longitud para rellenar
            case "Raqueta":
                vista.tallaLongitudLbl.setText("Longitud");
                break;
                //damos funcionalidad al boton cerrar aplicacion--> si presionamos se cierra la app tras un cuadro de dialogo si o no
            case "Cerrar":
                if (Util.botonCerrar("Desea cerrar?",null)==JOptionPane.OK_OPTION){
                    System.exit(0);
                }
            case "Limpiar":
                vaciarCampos();


        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        //solo ejecuto si el codigo valor se está ajustando
        if(e.getValueIsAdjusting()){
            Producto productoSeleccionado = (Producto) vista.list1.getSelectedValue();
            vista.idTxt.setText(productoSeleccionado.getId());
            vista.marcaTxt.setText(productoSeleccionado.getMarca());
            vista.colorTxt.setText(productoSeleccionado.getColor());
            vista.fechaCompraDPicker.setDate(productoSeleccionado.getFechaCompra());

            if(productoSeleccionado instanceof Raqueta){
                vista.raquetaRadioButton.doClick();
                vista.longitudTallaTxt.setText(String.valueOf(((Raqueta)productoSeleccionado).getLongitudRaqueta()));
            }else{
                vista.ropaRadioButton.doClick();
                vista.longitudTallaTxt.setText(String.valueOf(((Ropa)productoSeleccionado).getTalla()));
            }
        }

    }

    //listener de los radioButton y botones
    private void addActionListener(ActionListener listener){
        vista.raquetaRadioButton.addActionListener(listener);
        vista.ropaRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.cerrarBtn.addActionListener(listener);
        vista.btnLimpiar.addActionListener(listener);
    }
    //listener del frame
    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }
    //listener de la lista
    private void addListSelectionListener(ListSelectionListener listener){
        vista.list1.addListSelectionListener(listener);
    }

    //limpiar campos
    private void limpiarCampos(){
        vista.longitudTallaTxt.setText(null);
        vista.idTxt.setText(null);
        vista.marcaTxt.setText(null);
        vista.colorTxt.setText(null);
        vista.fechaCompraDPicker.setText(null);
        vista.idTxt.requestFocus();
    }

    //comprobar campos vacios
    private boolean hayCamposVacios(){
        if(vista.longitudTallaTxt.getText().isEmpty() ||
                vista.idTxt.getText().isEmpty() ||
                vista.marcaTxt.getText().isEmpty() ||
                vista.colorTxt.getText().isEmpty() ||
                vista.fechaCompraDPicker.getText().isEmpty()){
            return true;
        }
        return false;
    }

    //cargar datos en la lista
    public void refrescar(){
        vista.dlmProducto.clear();
        for(Producto unProducto : modelo.obtenerProductos()){
            vista.dlmProducto.addElement(unProducto);
        }
    }

    //metodo a traves del cual cerramos la interfaz gráfica de la X
    // aparece un cuadro de dialogo-- si la respuesta es si-- se cierra app
    //si respuesta es no---> no se cierra app
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea cerrar la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarDatosConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
         }
        }



//metodo por el cual nos lanza un mensaje al ejecutar la interfaz grafica
    //lanza mensaje ¿desea acceder al control de inventario?
    //opcion de si/no-- opcion si--> accede a la interfaz y puedes operar
                    //-- opcion no--> no accede a la interfaz y cierra la app por completo
    @Override
    public void windowOpened(WindowEvent e) {
        int resp = Util.mensajeEntrada("¿Desea acceder?", "Control de inventario");
        if (resp == JOptionPane.NO_OPTION) {
            System.exit(0);

        }

    }



    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    public void vaciarCampos(){
        vista.idTxt.setText("");
        vista.marcaTxt.setText("");
        vista.colorTxt.setText("");
        vista.fechaCompraDPicker.setText("");
        vista.longitudTallaTxt.setText("");
    }

}
