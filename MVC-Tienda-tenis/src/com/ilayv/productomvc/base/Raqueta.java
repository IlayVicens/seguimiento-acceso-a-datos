package com.ilayv.productomvc.base;

import com.ilayv.productomvc.base.Producto;

import java.time.LocalDate;


//clase Raqueta que hereda de la clase Producto
public class Raqueta extends Producto {
    //campo exclusivo de la clase Raqueta
    private double longitudRaqueta;

    //constructor unicamente para llamar a los campos heredados con el super();
    public Raqueta(){
        super();
    }

    //constructor donde pasamos los campos heredados de la clase Producto y añadimos el propio de la clase--> longitud
    public Raqueta(String id, String marca, String color, LocalDate fechaCompra, double longitudRaqueta) {
        super(id, marca, color, fechaCompra);
        this.longitudRaqueta = longitudRaqueta;
    }

    //creacion del setter y getter del campo longitudRaqueta
    public double getLongitudRaqueta() {
        return longitudRaqueta;
    }

    public void setLongitudRaqueta(double longitudRaqueta) {
        this.longitudRaqueta = longitudRaqueta;
    }

    //creación del metodo to String
    @Override
    public String toString() {
        return "Raqueta{" +
                "id=" + getId() +
                "marca=" + getMarca() +
                "color=" + getColor() +
                '}';
    }
}
