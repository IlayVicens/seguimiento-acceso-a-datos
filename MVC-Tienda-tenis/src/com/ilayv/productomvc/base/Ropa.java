package com.ilayv.productomvc.base;

import com.ilayv.productomvc.base.Producto;

import java.time.LocalDate;

//clase Ropa que hereda de Producto
public class Ropa extends Producto {

    //campo exclusivo de la clase Ropa-->talla
    private String talla;

    //constructor vacio llamando al super de la clase Producto
    public Ropa(){
        super();
    }

    //constructor con los campos de la clase padre-->Producto--> y añadimos el campo unico de la clase Ropa
    public Ropa(String id, String marca, String color, LocalDate fechaCompra, String talla) {
        super(id, marca, color, fechaCompra);
        this.talla = talla;
    }

    //creacion del getter y setter del campo Talla
    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    //generación del toString
    @Override
    public String toString() {
        return "Ropa{" +
                "id=" + getId() +
                "marca=" + getMarca() +
                "color=" + getColor() +
                '}';
    }
}
