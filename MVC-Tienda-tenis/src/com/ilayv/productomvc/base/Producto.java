package com.ilayv.productomvc.base;

import java.time.LocalDate;

public abstract class Producto {
    /*
    @author Ilay
     */

    //campos comunes a todas las clases que hereda la clase Producto
    private String id;
    private String marca;
    private String color;
    private LocalDate fechaCompra;

    //constructor vacio
    public Producto(){

    }

    //constructor donde pasamos las variables y les asignamos un nombre
    public Producto(String id, String marca, String color, LocalDate fechaCompra) {
        this.id = id;
        this.marca = marca;
        this.color = color;
        this.fechaCompra = fechaCompra;
    }

    //creacion de los getters y setters de los campos anteriores
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
