package com.ilayv.productomvc;

import com.github.lgooddatepicker.components.DatePicker;
import com.ilayv.productomvc.base.Producto;

import javax.swing.*;

//en esta clase aparecen todos los elementos graficos de la interfaz grafica..> botones, campos de texto, selector de fechas...
public class Ventana {
    private JPanel panel1;
    //public JLabel T;
    public JRadioButton raquetaRadioButton;
    public JRadioButton ropaRadioButton;
    public JTextField idTxt;
    public JTextField marcaTxt;
    public JTextField colorTxt;
    public JTextField longitudTallaTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public DatePicker fechaCompraDPicker;
    public JLabel tallaLongitudLbl;
    public JButton cerrarBtn;
    public JButton btnLimpiar;
    public JFrame frame;

    //Este componente nos permite presentar una lista de selección donde podemos escoger uno o varios elementos-- en mi caso de productos
    public DefaultListModel<Producto> dlmProducto;

    //constructor de Ventana donde asignamos el nombre de dicha ventana
    //añadimos el panel a la ventana
    //y hacemos visible dicha ventana para que aparezca al ejecutar la app
    public Ventana(){
        frame = new JFrame("ProductosMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        //creamos el metodo initComponents();
        initComponents();

    }

    private void initComponents(){
        //creamos el defaultListModel de Productos
        dlmProducto = new DefaultListModel<Producto>();
        //añadimos la lista de productos a la lista para que aparezcan los productos nuevos que añadamos
        list1.setModel(dlmProducto);
    }

}
