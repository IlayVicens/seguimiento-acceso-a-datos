import com.ilayv.productomvc.Ventana;
import com.ilayv.productomvc.gui.ProductosControlador;
import com.ilayv.productomvc.gui.ProductosModelo;



public class Principal {
    public static void main(String[] args) {
        //creamos la ventana del JFrame para que sea visible
        Ventana vista = new Ventana();
        //creamos la clase ProductosModelo
        ProductosModelo modelo = new ProductosModelo();
        //creamos la clase ProductosControlados y le pasamos como parametro la vista y el modelo
        ProductosControlador controlador = new ProductosControlador(vista,modelo);
    }
}
