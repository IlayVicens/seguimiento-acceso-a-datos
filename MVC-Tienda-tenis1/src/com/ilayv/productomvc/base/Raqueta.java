package com.ilayv.productomvc.base;

import com.ilayv.productomvc.base.Producto;

import java.time.LocalDate;

public class Raqueta extends Producto {
    private double longitudRaqueta;

    public Raqueta(){
        super();
    }

    public Raqueta(String id, String marca, String color, LocalDate fechaCompra, double longitudRaqueta) {
        super(id, marca, color, fechaCompra);
        this.longitudRaqueta = longitudRaqueta;
    }

    public double getLongitudRaqueta() {
        return longitudRaqueta;
    }

    public void setLongitudRaqueta(double longitudRaqueta) {
        this.longitudRaqueta = longitudRaqueta;
    }

    @Override
    public String toString() {
        return "Raqueta{" +
                "id=" + getId() +
                "marca=" + getMarca() +
                "color=" + getColor() +
                '}';
    }
}
