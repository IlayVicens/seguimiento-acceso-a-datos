package com.ilayv.productomvc.base;

import com.ilayv.productomvc.base.Producto;

import java.time.LocalDate;

public class Ropa extends Producto {

    private String talla;

    public Ropa(){
        super();
    }

    public Ropa(String id, String marca, String color, LocalDate fechaCompra, String talla) {
        super(id, marca, color, fechaCompra);
        this.talla = talla;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    @Override
    public String toString() {
        return "Ropa{" +
                "id=" + getId() +
                "marca=" + getMarca() +
                "color=" + getColor() +
                '}';
    }
}
