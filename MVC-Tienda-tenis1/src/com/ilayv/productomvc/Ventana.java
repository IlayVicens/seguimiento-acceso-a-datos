package com.ilayv.productomvc;

import com.github.lgooddatepicker.components.DatePicker;
import com.ilayv.productomvc.base.Producto;

import javax.swing.*;

public class Ventana {
    private JPanel panel1;
    //public JLabel T;
    public JRadioButton raquetaRadioButton;
    public JRadioButton ropaRadioButton;
    public JTextField idTxt;
    public JTextField marcaTxt;
    public JTextField colorTxt;
    public JTextField longitudTallaTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public DatePicker fechaCompraDPicker;
    public JLabel tallaLongitudLbl;
    public JButton cerrarBtn;
    public JFrame frame;

    public DefaultListModel<Producto> dlmProducto;

    public Ventana(){
        frame = new JFrame("ProductosMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents(){
        dlmProducto = new DefaultListModel<Producto>();
        list1.setModel(dlmProducto);
    }

}
