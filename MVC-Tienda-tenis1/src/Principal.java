import com.ilayv.productomvc.Ventana;
import com.ilayv.productomvc.gui.ProductosControlador;
import com.ilayv.productomvc.gui.ProductosModelo;



public class Principal {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        ProductosModelo modelo = new ProductosModelo();
        ProductosControlador controlador = new ProductosControlador(vista,modelo);
    }
}
